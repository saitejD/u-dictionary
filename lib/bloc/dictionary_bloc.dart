import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:u_dictionary/models/word_response.dart';
import 'package:u_dictionary/repo/word_repo.dart';

@immutable
abstract class DictionaryState {}

class NoWordSearchedState extends DictionaryState {}

class WordSearchingState extends DictionaryState {}

class NoWordFoundState extends DictionaryState {}

class WordSearchedState extends DictionaryState {
  final List<WordResponse> words;
  WordSearchedState(this.words);
}

class ErrorState extends DictionaryState {
  final Exception exception;
  ErrorState(this.exception);
}

@immutable
abstract class DictionaryEvent {}

class SearchWordEvent extends DictionaryEvent {}

class DictionaryBloc extends Bloc<DictionaryEvent, DictionaryState> {
  final WordRespository _respository;
  final queryController = TextEditingController();

  DictionaryBloc(this._respository) : super(NoWordSearchedState());

  @override
  Stream<DictionaryState> mapEventToState(DictionaryEvent event) async* {
    if (event is SearchWordEvent) {
      yield WordSearchingState();
      try {
        final words =
            await _respository.getWordsFromDictionary(queryController.text);
        if (words.length == 0) {
          yield NoWordFoundState();
          yield NoWordSearchedState();
        } else {
          print(words[0].meanings);
          yield WordSearchedState(words);
        }
      } on Exception catch (e) {
        print(e);
        yield ErrorState(e);
      }
    }
  }
  // Future getWordsSearched() async {
  //   emit(WordSearchingState());
  //   try {
  //     final words =
  //         await _respository.getWordsFromDictionary(queryController.text);

  //     if (words.length == 0) {
  //       emit(ErrorState(Exception("No word Found!")));
  //     } else {
  //       print(words[0].meanings);
  //       emit(WordSearchedState(words));
  //       emit(NoWordSearchedState());
  //     }
  //   } on Exception catch (e) {
  //     print(e);
  //     emit(ErrorState(e));
  //   }
  // }
}
