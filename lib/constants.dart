import 'package:flutter/material.dart';

const Color color1 = Color(0xFF040417);
const Color color2 = Color(0xFFc0384b);
const Color color3 = Color(0xFF4a4962);
const Color color4 = Color(0xFF6F728d);
const Color color5 = Color(0xFFd9dadc);
const Color color6 = Color(0xFFc2bcc5);
