import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:u_dictionary/constants.dart';
import 'package:u_dictionary/screens/home/home_screen.dart';
import 'bloc/dictionary_bloc.dart';
import 'repo/word_repo.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'U-Dictionary',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(        
        colorScheme: ColorScheme.light().copyWith(
          primary: color3,
          secondary: Colors.transparent,
        ),
      ),
      themeMode: ThemeMode.light,
      home: BlocProvider<DictionaryBloc>(
          child: HomeScreen(),
          create: (context) => DictionaryBloc(WordRespository()),),
    );
  }
}

// json object from dictionary api
// [
//   {
//     "word":"anxiety",
//     "phonetic":"aŋˈzʌɪəti","phonetics":[{"text":"aŋˈzʌɪəti","audio":"//ssl.gstatic.com/dictionary/static/sounds/20200429/anxiety--_gb_1.mp3"}],
//     "origin":"early 16th century: from French anxiété or Latin anxietas, from anxius (see anxious).",
//     "meanings":
//     [
//       {"partOfSpeech":"noun",
//     "definitions":[{"definition":"a feeling of worry, nervousness, or unease about something with an uncertain outcome.","example":"he felt a surge of anxiety",
//     "synonyms":["worry","concern","apprehension","apprehensiveness","consternation","uneasiness","unease","fearfulness","fear","disquiet","disquietude","perturbation","fretfulness","agitation","angst","nervousness","nerves","edginess","tension","tenseness","stress","misgiving","trepidation","foreboding","suspense","butterflies (in one's stomach)","the willies","the heebie-jeebies","the jitters","the shakes","the jumps","the yips","collywobbles","jitteriness","jim-jams","twitchiness","the (screaming) abdabs","Joe Blakes","worriment"],
//     "antonyms":[""],}]
//       }
//     ]
//   }
// ]