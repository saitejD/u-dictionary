import 'dart:io';
import 'package:u_dictionary/models/word_response.dart';
import 'package:u_dictionary/service/http_service.dart';

class WordRespository {
  Future<List<WordResponse>> getWordsFromDictionary(String query) async {
    try {
      final response = await HttpService.getRequest("en/$query");

      if (response.statusCode == 200) {
        final result = wordResponseFromJson(response.body);
        return result;
      } else {
        return [];
      }
    } on SocketException catch (e) {
      throw e;
    } on HttpException catch (e) {
      throw e;
    } on FormatException catch (e) {
      throw e;
    }
  }
}
