import 'package:flutter/material.dart';
import 'package:u_dictionary/constants.dart';
import 'package:u_dictionary/models/word_response.dart';
import 'package:just_audio/just_audio.dart';

class DetailsScreenRedesigned extends StatefulWidget {
  final WordResponse wordResponse;

  DetailsScreenRedesigned({required this.wordResponse});
  @override
  _DetailsScreenRedesignedState createState() =>
      _DetailsScreenRedesignedState();
}

class _DetailsScreenRedesignedState extends State<DetailsScreenRedesigned> {
  final _player = AudioPlayer();

  onScreenLoaded() async {
    try {
      // print(
      //     "audio uri ${widget.wordResponse.phonetics![0].audio!.replaceFirst('//', '')}");
      // await _player.setAsset('assets/O Prema.mp3');
      // await _player.setAudioSource(AudioSource.uri(
      //   Uri.parse(
      //       widget.wordResponse.phonetics![0].audio!.replaceFirst('//', '')),
      // ));
      try {
        await _player.setUrl(
            "${widget.wordResponse.phonetics![0].audio!.replaceFirst('//', '')}");
      } on PlayerException catch (e) {
        // iOS/macOS: maps to NSError.code
        // Android: maps to ExoPlayerException.type
        // Web: maps to MediaError.code
        print("Error code: ${e.code}");
        // iOS/macOS: maps to NSError.localizedDescription
        // Android: maps to ExoPlaybackException.getMessage()
        // Web: a generic message
        print("Error message: ${e.message}");
      } on PlayerInterruptedException catch (e) {
        // This call was interrupted since another audio source was loaded or the
        // player was stopped or disposed before this audio source could complete
        // loading.
        print("Connection aborted: ${e.message}");
      } catch (e) {
        // Fallback for all errors
        print(e);
      }
    } catch (e) {
      print("Error loading audio source: $e");
    }
  }

  @override
  void initState() {
    // print(widget.wordResponse.meanings);
    onScreenLoaded();
    super.initState();
  }

  @override
  void dispose() {
    _player.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      _player.stop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: color5,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 20),
                Card(
                  elevation: 10,
                  shadowColor: color3.withOpacity(0.9),
                  color: color3,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Container(
                    height: 150,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              widget.wordResponse.word,
                              style: TextStyle(
                                color: color5,
                                fontWeight: FontWeight.bold,
                                fontSize: 25,
                              ),
                            ),
                            SizedBox(width: 20),
                            Container(
                              height: 45,
                              width: 45,
                              decoration: BoxDecoration(
                                color: color2,
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Center(
                                child: IconButton(
                                  onPressed: () => _player.play(),
                                  icon: Icon(
                                    Icons.volume_up,
                                    color: color5,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 20),
                        Text(
                          widget.wordResponse.phonetics![0].text!,
                          style: TextStyle(
                            color: color2,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 40),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Text(
                    "Meanings",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontSize: 22,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: widget.wordResponse.meanings!.length,
                  itemBuilder: (context, index) {
                    List<String> synonyms = [];
                    List<String> antonyms = [];
                    widget.wordResponse.meanings![index].definitions!
                        .forEach((element) {
                      synonyms.addAll(element.synonyms!);
                    });
                    widget.wordResponse.meanings![index].definitions!
                        .forEach((element) {
                      antonyms.addAll(element.antonyms!);
                    });
                    return Card(
                      elevation: 1,
                      color: color6,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget
                                  .wordResponse.meanings![index].partOfSpeech!,
                              style: TextStyle(
                                color: color1,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            ListView.builder(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 5, vertical: 10),
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: widget.wordResponse.meanings![index]
                                          .definitions!.length <=
                                      4
                                  ? widget.wordResponse.meanings![index]
                                      .definitions!.length
                                  : 4,
                              itemBuilder: (context, ind) {
                                return Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "${ind + 1}. ",
                                            style: TextStyle(
                                              fontWeight: FontWeight.w800,
                                            ),
                                          ),
                                          Expanded(
                                            child: Text(
                                              "${widget.wordResponse.meanings![index].definitions![ind].definition!}",
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w800,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: 5),
                                      widget.wordResponse.meanings![index]
                                                  .definitions![ind].example !=
                                              null
                                          ? Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 20.0),
                                              child: Text(
                                                "${widget.wordResponse.meanings![index].definitions![ind].example!}",
                                                style: TextStyle(
                                                    fontStyle:
                                                        FontStyle.italic),
                                              ),
                                            )
                                          : SizedBox.shrink(),
                                    ],
                                  ),
                                );
                              },
                            ),
                            synonyms.isNotEmpty
                                ? Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Synomys",
                                          style: TextStyle(
                                            // color: color5,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w800,
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 20.0),
                                          child: Text("$synonyms"),
                                        ),
                                      ],
                                    ),
                                  )
                                : SizedBox.shrink(),
                            SizedBox(height: 10),
                            antonyms.isNotEmpty
                                ? Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Antonyms",
                                          style: TextStyle(
                                            // color: color5,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w800,
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 20.0),
                                          child: Text("$antonyms"),
                                        )
                                      ],
                                    ),
                                  )
                                : SizedBox.shrink(),
                          ],
                        ),
                      ),
                    );
                  },
                ),
                SizedBox(height: 10),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
