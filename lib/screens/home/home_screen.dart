import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:u_dictionary/bloc/dictionary_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:u_dictionary/screens/words_list/words_list_screen.dart';
import 'package:u_dictionary/screens/word_of_day/word_of_day_screen.dart';
import '../../constants.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = context.watch<DictionaryBloc>();
    return BlocListener<DictionaryBloc, DictionaryState>(
      listener: (context, state) {
        if (state is WordSearchedState) {
          Navigator.push(
            context,
            CupertinoPageRoute(
              builder: (context) => ListScreen(state.words),
            ),
          );
        }
        if (state is NoWordFoundState) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text(
                  "We couldn't find the word you searched! Please check your spelling."),
            ),
          );
        }
        if (state is ErrorState) {
          if (state.exception is SocketException) {
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                content: Text(
                    "Your are not connected to Internet. Please check your network connection."),
              ),
            );
          } else {
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(
                content: Text("Something went worng! Please search again."),
              ),
            );
          }
        }
      },
      bloc: bloc,
      child: bloc.state is WordSearchingState
          ? getLoadingWidget()
          : getDictionaryFormWidget(context),
    );
  }

  Widget getDictionaryFormWidget(BuildContext context) {
    final bloc = context.watch<DictionaryBloc>();
    return Scaffold(
      backgroundColor: color5,
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            Spacer(),
            Text(
              "U-Dictionary",
              style: TextStyle(
                color: color3,
                fontSize: 34,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 10),
            Text(
              "Search any word you want quickly",
              style: TextStyle(
                color: Colors.white70,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 32),
            TextField(
              controller: bloc.queryController,
              onSubmitted: (value) {
                if (bloc.queryController.text.isNotEmpty)
                  bloc..add(SearchWordEvent());
              },
              decoration: InputDecoration(
                hintText: "Search by word",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  // borderSide: BorderSide(color: Colors.transparent),
                ),
                fillColor: Colors.white60,
                filled: true,
                prefixIcon: Icon(Icons.search_outlined, size: 30),
                suffixIcon: Icon(Icons.mic_outlined, size: 35),
              ),
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Show History",
                    style: TextStyle(
                      color: color3,
                      fontWeight: FontWeight.bold,
                    )),
                Icon(
                  Icons.keyboard_arrow_down,
                  color: color3,
                )
              ],
            ),
            Spacer(),
            InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  CupertinoPageRoute(
                    builder: (context) => WordOfDayScreen(),
                  ),
                );
              },
              child: Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                  color: color2,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Icon(
                  Icons.save,
                  color: color5,
                ),
              ),
            ),
            SizedBox(height: 10),
          ],
        ),
      ),
    );
  }

  Widget getLoadingWidget() {
    return Scaffold(
      backgroundColor: color5,
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget getErrorWidget() {
    return Scaffold(
      backgroundColor: color5,
      body: Center(
          child: Text(
        "Something went wrong. Please Check your Internet Connection!",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
      )),
    );
  }
}
