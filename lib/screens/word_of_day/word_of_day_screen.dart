import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../constants.dart';

class WordOfDayScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: color5,
      body: SafeArea(
        child: Column(
          children: [
            SizedBox(height: 30),
            SvgPicture.asset(
              "assets/book_lover.svg",
              height: 210,
              fit: BoxFit.cover,
            ),
            SizedBox(height: 20),
            Text(
              "Word of the day",
              style: TextStyle(
                  fontSize: 34, color: color3, fontWeight: FontWeight.bold),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Text(
                "Build your Vocabulary with new words and definations every day",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20,
                  color: color3,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            Spacer(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Card(
                color: color3,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                elevation: 5,
                shadowColor: color1.withOpacity(0.3),
                child: Container(
                  height: 200,
                  padding: EdgeInsets.all(20.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        // crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Ambisinister",
                            style: TextStyle(
                              color: color5,
                              fontWeight: FontWeight.bold,
                              fontSize: 32,
                            ),
                          ),
                          SizedBox(width: 20),
                          Container(
                            height: 45,
                            width: 45,
                            decoration: BoxDecoration(
                              color: color2,
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Center(
                              child: IconButton(
                                onPressed: () {},
                                icon: Icon(
                                  Icons.volume_up,
                                  color: color5,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 20),
                      Text(
                        "Show IPA  [am-bi-sin-uh-ster]",
                        style: TextStyle(
                          color: color2,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        "clumsy or unskilfull with both hands",
                        style: TextStyle(
                          color: color5,
                        ),
                      ),
                      Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Show More",
                            style: TextStyle(
                              color: color2,
                            ),
                          ),
                          Icon(
                            Icons.keyboard_arrow_down,
                            color: color2,
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
            Spacer(
              flex: 3,
            ),
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                height: 56,
                width: 56,
                decoration: BoxDecoration(
                  color: color2,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Icon(
                  Icons.close,
                  color: color5,
                  size: 30,
                ),
              ),
            ),
            SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
