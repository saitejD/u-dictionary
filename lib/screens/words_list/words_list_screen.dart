import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:u_dictionary/models/word_response.dart';
import 'package:u_dictionary/screens/details/word_details_screen.dart';

import '../../constants.dart';

class ListScreen extends StatelessWidget {
  final List<WordResponse> words;
  ListScreen(this.words);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: color5,
      body: SafeArea(
        child: ListView.builder(
          itemBuilder: (context, index) => ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
            title: Text(
              "${index + 1}. ${words[index].word}",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
            onTap: () => Navigator.push(
              context,
              CupertinoPageRoute(
                builder: (context) =>
                    DetailsScreenRedesigned(wordResponse: words[index]),
              ),
            ),
          ),
          itemCount: words.length,
        ),
      ),
    );
  }
}
