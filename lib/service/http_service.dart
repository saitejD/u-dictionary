import 'package:http/http.dart' as http;

class HttpService {
  static final baseUrl = "https://api.dictionaryapi.dev/api/v2/entries/";

  static Future<http.Response> getRequest(endPoint) async {
    http.Response response;
    final Uri url = Uri.parse("$baseUrl$endPoint");

    try {
      response = await http.get(url);
    } on Exception catch (e) {
      print("exception from api service ${e.toString()}");
      throw e;
    }
    print("service response ${response.body}");

    return response;
  }
}
